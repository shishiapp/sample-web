import React, {Component} from 'react';
import appReducers from './reducers'
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import {Route, HashRouter as Router} from 'react-router-dom';
import Root from "./components/Root";
import './App.css';


class App extends Component {

  store = createStore(appReducers)

  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {

    return (
        <Provider store={this.store}>
          <Router>
            <div>
              <Route path={'/'} component={Root}/>
            </div>
          </Router>
        </Provider>
    );

  }
}


export default App;