
export const SELECT_TRIPLET = 'SELECT_TRIPLET';
export const UPDATE_LOOK = 'UPDATE_LOOK';


export function updateLook() {
    return {
        type: UPDATE_LOOK
    }
}

export function selectTriplet(triplet) {
    return {
        type: SELECT_TRIPLET,
        triplet: triplet
    }
}