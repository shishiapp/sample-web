import React, {Component} from 'react';
import {Row, Col, List} from 'antd';
import * as Actions from "../actions";
import {connect} from "react-redux";

const ShiShiSdk = window.trycore.ShiShiSdk;
const ColorSwatchCanvas = window.trycore.ColorSwatchCanvas;
const ColorSwatch = window.trycore.ColorSwatch;



class LookDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            triplets: []
        };

    }

    componentDidMount() {

        let lookJson = {
            tryon_groups: [
                {items: [472, 473, 471], parameter: 0, style: 46, category: 2},
                {items: [472], parameter: 50, style: 49, category: 2},
                {items: [528], parameter: 0, style: 18, category: 5}]
        };

        ShiShiSdk.getInstance().getTripletsOfLookJson(lookJson, (triplets, more) => {

            this.setState({triplets: triplets});
            this.applyThisLook();
        });

    }



    applyThisLook() {

        let triplets = this.state.triplets;

        ShiShiSdk.getInstance().removeAllMakeup();

        triplets.forEach(triplet => {
            if(triplet != null) {
                ShiShiSdk.getInstance().putTripletInCurrentLook(triplet, false);
            }
        });

        this.props.dispatch(Actions.updateLook());
    }



    swatchCanvasInitialized() {
        this.setState({swatchCanvasInitialized: true});
    }


    getProductCell(triplet) {

        let tryOnGroup = triplet[0];
        let concept = triplet[2];
        let productItem = triplet[1];

        let category = ShiShiSdk.getInstance().getCategory(tryOnGroup.category);

        return (

            <div style={{width:'100%'}}>

                <p className={'look-product-category-text'}>{category.name}</p>

                <Row className={'look-product-container'}>

                    <Col xs={12}>
                        <img className={'look-product-image'} src={productItem.image}/>
                    </Col>

                    <Col xs={12}>
                        <p className={'look-product-info-text'}> {concept.name}</p>

                        <Row>
                            <Col xs={12}>
                                <ColorSwatch className = {"look-color-swatch-selection-table"} canvas = {this.swatchCanvas} tryOnItems = {tryOnGroup.tryon_item_objects}/>
                            </Col>
                            <Col xs={12}>
                                <img className={'look-product-style-image'} src={tryOnGroup.style.image}/>
                            </Col>

                        </Row>


                    </Col>
                </Row>

            </div>


        );
    }



    render() {

        return (

            <Row>

                <ColorSwatchCanvas ref={ref => this.swatchCanvas = ref} width={32} height={32} finishInit={() => this.swatchCanvasInitialized()}/>

                <Col xs={24} sm={24} md={24} lg={24} xl={11}>


                    <List
                        itemLayout="horizontal"
                        dataSource={this.state.triplets}

                        renderItem={triplet => (
                            <List.Item>
                                {this.getProductCell(triplet)}
                            </List.Item>
                        )}
                    />

                </Col>

                <Col xs={24} sm={24} md={24} lg={24} xl={{span: 10, offset: 2}}>

                </Col>
            </Row>


        );
    }
}

const mapStateToProps = store => {
    return {
    }
};


export default LookDetail = connect(mapStateToProps)(LookDetail);
