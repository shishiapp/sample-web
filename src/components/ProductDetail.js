import React, {Component} from 'react';
import {Row, Col, List, Card} from 'antd';
import {connect} from "react-redux";
import * as Actions from "../actions";
import unavailable from '../icon/unavailable.jpg';



const ShiShiSdk = window.trycore.ShiShiSdk;
const ColorSwatchCanvas = window.trycore.ColorSwatchCanvas;
const ColorSwatch = window.trycore.ColorSwatch;


const nullTriplet = [];



class ProductDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            triplets: [],
            concept: null
        };

    }

    componentDidMount() {

        ShiShiSdk.getInstance().getTripletsOfProductItems([275, 276, 277], (triplets, more) => {

            if (triplets.length > 0) {

                let triplet = triplets[0];
                triplets.unshift(nullTriplet);
                let tripletOfLook = ShiShiSdk.getInstance().createTripletOfLook(triplet);

                ShiShiSdk.getInstance().removeAllMakeup();
                ShiShiSdk.getInstance().putTripletInCurrentLook(tripletOfLook, true);
                ShiShiSdk.getInstance().setSelectedTripletOfLook(tripletOfLook);

                this.setState({triplets: triplets, concept: triplet[2]});


                this.props.dispatch(Actions.updateLook());
            }

        }, () => {});
    }


    handleSwatchClick(e, triplet) {


        if (triplet !== nullTriplet) {
            let tripletOfLook = ShiShiSdk.getInstance().createTripletOfLookForReplacement(triplet);

            ShiShiSdk.getInstance().removeAllMakeup();
            ShiShiSdk.getInstance().putTripletInCurrentLook(tripletOfLook, true);
            ShiShiSdk.getInstance().setSelectedTripletOfLook(tripletOfLook);

            this.props.dispatch(Actions.updateLook());

        } else {
            if(ShiShiSdk.getInstance().getSelectedTripletOfLook()) {
                ShiShiSdk.getInstance().removeAllMakeup();
                this.props.dispatch(Actions.updateLook());
            }
        }


    }

    swatchCanvasInitialized() {
        this.setState({swatchCanvasInitialized: true});
    }

    selected(triplet) {
        let selectedTriplet = ShiShiSdk.getInstance().getSelectedTripletOfLook();
        if (selectedTriplet == null) {
            if (triplet === nullTriplet) {
                return {borderColor: 'black'};
            }
        } else {
            if (triplet !== nullTriplet && selectedTriplet[0].tryOnGroup === triplet[0]) {
                return {borderColor: 'black'};
            }
        }

        return {borderColor: 'white'};

    }

    renderProductName() {
        let productName = this.state.currentProduct.name;
        if (productName && productName.length > 0 && productName != this.state.concept.name) {
            return (<p className={'product-info'}> {productName} </p>);
        } else {
            return (<p className={'product-info'}><br/></p>);
        }
    }


    renderSwatchCell(swatchCanvas, triplet) {
        if (triplet === nullTriplet) {
            return <img src={unavailable} style={{width: '100%', height: '100%'}}/>;

        } else {
            return <ColorSwatch canvas = {swatchCanvas} tryOnItems = {triplet[0].tryOnItems}/>;
        }
    }

    renderProductItemInfo() {

        let concept = this.state.concept;
        let productItemInfo = <div/>;
        if (concept) {

            if (this.state.currentProduct) {
                productItemInfo = (
                    <div style={{marginLeft: 20, marginRight: 20}}>
                        <div className={'product-image-container'}>
                            <img className={'product-image'} src={this.state.currentProduct.image}/>
                        </div>

                        <div>
                            {this.renderProductName()}
                        </div>
                        <br/>
                        <div>
                            <p>
                                {concept.description}
                                <br/>
                                {concept.ingredient_effect}
                                <br/>
                                {concept.howto}
                            </p>

                        </div>
                    </div>
                )
            }
        }
        return productItemInfo;

    }



    render() {


        let concept = this.state.concept;

        if (concept) {

            return (

                <Row>
                    <Col xs={24} sm={24} md={24} lg={24} xl={11}>

                        <div>
                            <p style={{fontSize: 'large'}}>{concept.name}</p>
                            <p> {concept.tag_line} </p>
                        </div>

                        <ColorSwatchCanvas ref={ref => this.swatchCanvas = ref} width={44} height={44} finishInit={() => this.swatchCanvasInitialized()}/>

                        <List
                            grid={{gutter: 16, xs: 6, sm: 8, md: 6, lg: 8, xl: 6, xxl: 8}}
                            dataSource={this.state.triplets}

                            renderItem={triplet => (
                                <List.Item>
                                    <Card className={'swatch-card'} style={this.selected(triplet)} hoverable
                                          onClick={((e) => this.handleSwatchClick(e, triplet))}>
                                        {this.renderSwatchCell(this.swatchCanvas, triplet)}
                                    </Card>
                                </List.Item>
                            )}
                        />

                    </Col>

                    <Col xs={24} sm={24} md={24} lg={24} xl={{span: 10, offset: 2}}>

                        {this.renderProductItemInfo()}

                    </Col>
                </Row>


            );
        } else {

            return (<div/>);
        }
    }
}

const mapStateToProps = store => {
    return {
        currentLook: store.currentLook
    }
};


export default ProductDetail = connect(mapStateToProps)(ProductDetail);
