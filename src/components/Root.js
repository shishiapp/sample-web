import React, {Component} from 'react';
import {Row, Col, Button, Layout} from 'antd';
import logo from '../icon/logo.jpg';
import Swatch from "./Swatch";
import {Route, Link, Switch, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import ProductDetail from "./ProductDetail";
import LookDetail from "./LookDetail";

const ButtonGroup = Button.Group;


const ShiShiSdk = window.trycore.ShiShiSdk;
const MakeupView = window.trycore.MakeupView;


class Root extends Component {
    constructor(props) {
        super(props);

        this.handleCaptureButtonClick = this.handleCaptureButtonClick.bind(this);
        this.handleDownloadButtonClick = this.handleDownloadButtonClick.bind(this);
        this.handleBeautifyButtonClick = this.handleBeautifyButtonClick.bind(this);

        this.state = {
            beautifyEnabled: true,
            captureImage: false
        };
    }


    componentDidMount() {

        ShiShiSdk.getInstance().smoothLevel = 2;
        ShiShiSdk.getInstance().morphLevel = 2;


        ShiShiSdk.getInstance().initialize("https://sg.shishiapp.com", false, () => {
        });
    }

    updateMakeup() {
        if (this.makeupView != null) {
            ShiShiSdk.getInstance().syncAllMakeup(this.makeupView);
            ShiShiSdk.getInstance().syncBeautify(this.makeupView);
        }
    }

    updateButtons() {

        let captureButton =
            (<Button onClick={this.handleCaptureButtonClick}>Capture Image</Button>);

        if (this.state.captureImage) {
            captureButton =
                (<Button onClick={this.handleDownloadButtonClick}>Download Image</Button>);
        }


        let beautifyButton = (<Button onClick={this.handleBeautifyButtonClick}>Beautify OFF</Button>);

        if (this.state.beautifyEnabled) {
            beautifyButton = (<Button onClick={this.handleBeautifyButtonClick}>Beautify ON</Button>);
        }

        return (<ButtonGroup className={'makeup-action-bar'}>
            {beautifyButton}
            {captureButton}
        </ButtonGroup>);

    }


    handleBeautifyButtonClick() {


        let beautifyEnabled = !this.state.beautifyEnabled;


        if (beautifyEnabled) {
            ShiShiSdk.getInstance().smoothLevel = 2;
            ShiShiSdk.getInstance().morphLevel = 2;
        } else {
            ShiShiSdk.getInstance().smoothLevel = 0;
            ShiShiSdk.getInstance().morphLevel = 0;
        }


        this.setState({beautifyEnabled: beautifyEnabled});
    }


    handleCaptureButtonClick() {
        this.makeupView.captureImage(true);
        this.setState({captureImage: true});
    }

    handleDownloadButtonClick() {
        this.makeupView.downloadImage("downloadedImage.jpg");
        this.makeupView.captureImage(false);
        this.setState({captureImage: false});
    }



    render() {

        this.updateMakeup();


        let pathname = this.props.location.pathname;

        let productProps = {};
        let lookProps = {};

        if (pathname.startsWith("/product")) {
            productProps = {fontWeight: "bold", textDecoration: "underline"};
            lookProps = {fontWeight: "lighter"};
        } else if (pathname.startsWith("/look")) {
            lookProps = {fontWeight: "bold", textDecoration: "underline"};
            productProps = {fontWeight: "lighter"};
        }

        return (
            <Layout style={{background: '#fff'}}>

                <Layout className="header" style={{background: '#E53465'}}>

                    <Row xs={3}>

                        <Col xs={4}>

                            <div className="logo">
                                <img src={logo} className="resize_fit_center"/>
                            </div>

                        </Col>
                        <Col xs={6}>
                        </Col>

                        <Col xs={4}>

                            <Link to={'/product'} style={{color: 'white', ...productProps}}>
                                PRODUCT
                            </Link>
                        </Col>

                        <Col xs={4}>
                            <Link to={'/look'} onClick={this.productClick} style={{color: 'white', ...lookProps}}>
                                LOOK
                            </Link>
                        </Col>


                        <Col xs={5}/>

                    </Row>


                </Layout>

                <div>

                    <Row justify="center">

                        <Col xs={24} sm={24} md={12} lg={12} xl={18}>

                            <Switch>

                                <Route path={'/product'} component={ProductDetail}/>
                                <Route path={'/look'} component={LookDetail}/>
                                <Redirect from="/" to="/product"/>
                            </Switch>

                        </Col>

                        <Col xs={24} sm={24} md={12} lg={12} xl={6} style={{background: '#fff'}}>

                            <div style={{marginTop: "24px"}}>

                            <MakeupView licenseLocation={"/"} ref={ref => this.makeupView = ref} />

                            {this.updateButtons()}

                            <div className={'style-container'}>
                                <Swatch/>
                            </div>

                            </div>


                        </Col>
                    </Row>
                </div>

            </Layout>
        );
    }
}

const mapStateToProps = store => {
    return {
        currentLook: store.currentLook
    }
};

export default Root = connect(mapStateToProps)(Root);
