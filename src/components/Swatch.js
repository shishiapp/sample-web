import React, {Component} from 'react';
import {List, Card, Radio} from 'antd';
import * as Actions from "../actions";
import {connect} from "react-redux";

const ShiShiSdk = window.trycore.ShiShiSdk;

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class Swatch extends Component {
    constructor(props) {
        super(props);

        this.handleStyleClick = this.handleStyleClick.bind(this);

        this.state = {
        }
    }

    componentDidMount() {

    }

    renderStyleCell(style) {

       return ( <img className={'style-card-image'} src={style.image}>

        </img>);

    }


    handleStyleClick(e, style) {

        ShiShiSdk.getInstance().setStyleOfSelectedTripletOfLook(style);

        this.props.dispatch(Actions.updateLook());
    }

    selected(style) {
        let selectedTriplet = ShiShiSdk.getInstance().getSelectedTripletOfLook();
        if (selectedTriplet && selectedTriplet[0].style.pk === style.pk) {
            return {borderColor: 'black'};
        } else {
            return {borderColor: 'white'};
        }
    }

    onChange = (e) => {
        let parameter = 50;

        switch(e.target.value) {
            case 'thin':
                parameter = 0;
                break;
            case 'normal':
                parameter = 50;
                break;
            case 'thick':
                parameter = 100;
                break;
            default:
                break;
        }

        ShiShiSdk.getInstance().setStyleParameterOfSelectedTripletOfLook(parameter);

    };

    getParameterButtons() {
        let selectedTriplet = ShiShiSdk.getInstance().getSelectedTripletOfLook();

        if (selectedTriplet == null) {
            return null;
        }

        let category = selectedTriplet[0].category;
        if (ShiShiSdk.getInstance().supportStyleParameter(category)) {

            let parameter = ShiShiSdk.getInstance().getStyleParameterOfCurrentTripletOfLook();

            let value = "normal";
            if (0 <= parameter && parameter <= 30) {
                value = "thin";
            } else if (30 < parameter && parameter <= 60) {
                value = "normal";
            } else if (60 < parameter && parameter <= 100) {
                value = "thick";
            }


            return (
                <RadioGroup onChange={this.onChange} value={value} defaultValue={"normal"}>
                    <RadioButton value="thin">Thin</RadioButton>
                    <RadioButton value="normal">Normal</RadioButton>
                    <RadioButton value="thick">Thick</RadioButton>
                </RadioGroup>
            );

        } else {
            return null;
        }
    }



    render() {

        let selectedTriplet = ShiShiSdk.getInstance().getSelectedTripletOfLook();

        if (selectedTriplet == null) {
            return <div/>;
        }

        let tryOnGroupOfLook = selectedTriplet[0];
        let tryOnGroup = tryOnGroupOfLook.tryOnGroup;
        let styles = tryOnGroup.styles;

        return (

            <div>

                {this.getParameterButtons()}

                <List
                    grid={{gutter: 16, xs: 4, sm: 4, md: 4, lg: 4, xl: 4, xxl: 4}}
                    dataSource={styles}

                    renderItem={style => (
                        <List.Item>
                            <Card className={'style-card'} style={this.selected(style)} hoverable
                                  onClick={((e) => this.handleStyleClick(e, style))}>
                                {this.renderStyleCell(style)}
                            </Card>
                        </List.Item>
                    )}
                />
            </div>

        );

    }
}

const mapStateToProps = store => {
    return {
        currentLook: store.currentLook
    }
};


export default Swatch = connect(mapStateToProps)(Swatch);