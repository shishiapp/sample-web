import { combineReducers } from 'redux'
import * as Actions from './actions'


function currentLook(state = [], action) {

    let newState = Object.assign([], state);
    switch (action.type) {
        case Actions.UPDATE_LOOK:
            newState = [];
            return newState;
        default:
            return state;
    }
}


const appReducers = combineReducers({
    currentLook
})

export default appReducers